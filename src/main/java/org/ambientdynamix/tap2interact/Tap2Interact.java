package org.ambientdynamix.tap2interact;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.ClipDescription;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.NfcA;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.*;
import org.ambientdynamix.api.application.*;
import org.ambientdynamix.contextplugins.ambientcontrol.ControlConnectionManager;
import org.ambientdynamix.contextplugins.ambientcontrol.IControlMessage;
import org.ambientdynamix.contextplugins.ambientmedia.IMediaDeviceInfo;
import org.ambientdynamix.contextplugins.ambientmedia.IMediaDeviceInfoList;

import java.util.*;

public class Tap2Interact extends Activity {


    private final String TAG = this.getClass().getSimpleName();
    private ArrayList<String> tapSession;
    private ContextHandler contextHandler;
    private DynamixFacade dynamix;
    private boolean tappingInProcess;
    private long lastTap;
    private final int TAP_SESSION_TIMEOUT = 20000;
    private Handler handler = new Handler();
    private TextView notifications;
    private static Map<String, String> ids;
    private NfcAdapter mNfcAdapter;
    private Map<String, ContextSupportInfo> activeReceiversMap;
    private ArrayList<String> activeReceivers;

    static {
        ids = new HashMap<String, String>();
        ids.put("05381E58D1B0C1", "org.ambientdynamix.contextplugins.hueplugin");
        ids.put("0531ACF103B0C1", "org.ambientdynamix.contextplugins.spheronative");
        ids.put("053F9339D1B0C1", "org.ambientdynamix.contextplugins.spheronative");
        ids.put("053D007F90B0C1", "org.ambientdynamix.contextplugins.ardrone");
        ids.put("053B7CF103B0C1", "org.ambientdynamix.contextplugins.wemoplugin");
        ids.put("053F8BCC91B0C1", "org.ambientdynamix.contextplugins.ambientmedia");
        ids.put("0539A78562B0C1", "org.ambientdynamix.contextplugins.myoplugin");
    }

    private PendingIntent mPendingIntent;
    private IntentFilter[] mFilters;
    private ListView listView;
    private ArrayAdapter<String> adapter;

    /**
     * Called when the activity is first created.
     *
     * @param savedInstanceState If the activity is being re-initialized after
     *                           previously being shut down then this Bundle contains the data it most
     *                           recently supplied in onSaveInstanceState(Bundle). <b>Note: Otherwise it is null.</b>
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        activeReceivers = new ArrayList<String>();
        activeReceiversMap = new HashMap<String, ContextSupportInfo>();

        // Create a generic PendingIntent that will be deliver to this activity. The NFC stack
        // will fill in the intent with the details of the discovered tag before delivering to
        // this activity.
        mPendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

        // Setup intent filters for all types of NFC dispatches to intercept all discovered tags
        IntentFilter ndef = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        try {
            ndef.addDataType("*/*");
        } catch (IntentFilter.MalformedMimeTypeException e) {
            throw new RuntimeException("fail", e);
        }
        IntentFilter tech = new IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED);
        tech.addDataScheme("vnd.android.nfc");
        tech.addCategory(Intent.CATEGORY_DEFAULT);
        try {
            tech.addDataType(ClipDescription.MIMETYPE_TEXT_PLAIN);
        } catch (IntentFilter.MalformedMimeTypeException e) {
            throw new RuntimeException("Check your mime type.");
        }
        mFilters = new IntentFilter[]{
                ndef,
                tech,
                new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED),
        };

        tapSession = new ArrayList<String>();
        setContentView(R.layout.activity_main);
        notifications = (TextView) findViewById(R.id.notifications);
        Button done = (Button) findViewById(R.id.done);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tapCompleted();
            }
        });
        Button cancel = (Button) findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                done("Canceled Tap Session");
            }
        });

        Button stopAll = (Button) findViewById(R.id.stopAll);
        stopAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopControlling();
            }
        });
        listView = (ListView) findViewById(R.id.activeReceivers);
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, activeReceivers);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {

            }
        });

        SwipeDismissListViewTouchListener touchListener = new SwipeDismissListViewTouchListener(
                listView,
                new SwipeDismissListViewTouchListener.DismissCallbacks() {
                    @Override
                    public boolean canDismiss(int position) {
                        return true;
                    }

                    @Override
                    public void onDismiss(ListView listView,
                                          int[] reverseSortedPositions) {
                        for (int reverseSortedPosition : reverseSortedPositions) {
                            stopControlling(activeReceivers.get(reverseSortedPosition));
                        }
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                adapter.notifyDataSetChanged();
                            }
                        });
                    }
                });
        touchListener.setEnabled(true);
        listView.setOnTouchListener(touchListener);

        handleIntent(getIntent());
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable ex) {
                Log.e(TAG,"Thread " + thread.getName() + " (" + thread.getId() + ") threw exception");
                ex.printStackTrace();
            }
        });

        /**Button button = (Button)findViewById(R.id.RegisterPYr);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (contextHandler == null || !DynamixConnector.isConnected()) {
                    contextHandler = null;
                    try {
                        ISessionCallback.Stub sessionCallback = new ISessionCallback.Stub() {
                            @Override
                            public void onSuccess(DynamixFacade iDynamixFacade) throws RemoteException {
                                dynamix = iDynamixFacade;
                                dynamix.createContextHandler(new ContextHandlerCallback() {
                                    @Override
                                    public void onSuccess(ContextHandler handler) throws RemoteException {
                                        contextHandler = handler;
                                        try {
                                            Bundle bundle = new Bundle();
                                            ArrayList<String> commands = new ArrayList<String>();
                                            commands.add(Commands.SENSOR_TOGGLE);
                                            bundle.putStringArrayList("REQUESTED_COMMANDS",commands);
                                            handler.addContextSupport("org.ambientdynamix.contextplugins.spheronative",IControlMessage.CONTEXT_TYPE,bundle,new ContextListener(){
                                                @Override
                                                public void onContextResult(ContextResult result) throws RemoteException {
                                                    Log.i(TAG,"received: " + result.getIContextInfo().getContextType());
                                                    if(result.getContextType().equals(IControlMessage.CONTEXT_TYPE) && result.hasIContextInfo() && result.getIContextInfo() instanceof PYRSensor){
                                                        PYRSensor sensorDate = (PYRSensor)result.getIContextInfo();
                                                        Log.i(TAG,"pitc/yaw/roll: " + sensorDate.getPitch() +"/"+sensorDate.getYaw()+"/"+sensorDate.getRoll());
                                                    }
                                                }
                                            });
                                        } catch (RemoteException e) {

                                        }
                                    }

                                    @Override
                                    public void onFailure(String message, int errorCode) throws RemoteException {
                                        done("failure to connect to create Context Handler");
                                        Log.e(TAG, message);
                                    }
                                });
                            }

                            @Override
                            public void onFailure(String s, int i) throws RemoteException {
                                done("failure to connect to Dynamix");
                                Log.e(TAG, s);
                            }
                        };
                        if (!DynamixConnector.isConnected())
                            DynamixConnector.openConnection(Tap2Interact.this, true, null, sessionCallback);
                        else if (dynamix != null)
                            sessionCallback.onSuccess(dynamix);
                    } catch (RemoteException e) {
                        done("Unknown Error: " + e);
                        e.printStackTrace();
                    }
                }
            }
        });*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(org.ambientdynamix.tap2interact.R.menu.main, menu);
        return true;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        Log.i(TAG, "started by NFC");
        String action = intent.getAction();
        if (NfcAdapter.ACTION_TECH_DISCOVERED.equals(action) || NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)) {

            // In case we would still use the Tech Discovered Intent
            final Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            String[] techList = tag.getTechList();
            NfcA tagNfcA = NfcA.get(tag);
            Log.i(TAG, "sensed TAG with ID: " + getIdFrombytes(tag.getId()));
            if (tappingInProcess && System.currentTimeMillis() - lastTap < TAP_SESSION_TIMEOUT) {
                lastTap = System.currentTimeMillis();
            } else {
                newSession();
            }
            final String pluginId = ids.get(getIdFrombytes(tag.getId()));
            if (pluginId != null) {
                tapSession.add(pluginId);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        notifications.setText("Discovered: " + pluginId);
                    }
                });
            } else {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        notifications.setText("Unknown Device Id");
                    }
                });
            }
        }
    }

    List<Thread> pendingThreads = new Vector<Thread>();

    private void done(final String message) {
        resetSession();
        for (Thread pendingThread : pendingThreads) {
            pendingThread.interrupt();
        }
        pendingThreads.clear();
        handler.post(new Runnable() {
            @Override
            public void run() {
                notifications.setText(message);
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(3000);
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    notifications.setText("Start new interaction by tapping a receiver");
                                }
                            });
                        } catch (InterruptedException e) {

                        }
                    }
                });
                thread.start();
                pendingThreads.add(thread);
            }
        });
    }

    private void resetSession() {
        tapSession.clear();
        tappingInProcess = false;
        lastTap = System.currentTimeMillis();
    }

    private void newSession() {
        tapSession.clear();
        tappingInProcess = true;
        lastTap = System.currentTimeMillis();
    }


    private void tapCompleted() {
        tappingInProcess = false;
//        tapSession.clear();
//        tapSession.add("org.ambientdynamix.contextplugins.ambientmedia");
//        tapSession.add("org.ambientdynamix.contextplugins.hueplugin");
//        tapSession.add("org.ambientdynamix.contextplugins.myoplugin");
//        tapSession.add("org.ambientdynamix.contextplugins.spheronative");
        handler.post(new Runnable() {
            @Override
            public void run() {
                notifications.setText("Selection completed loading interaction");
            }
        });
        if (tapSession.size() >= 2) {
            final String receiverId = tapSession.get(0);
            connect(receiverId, new ContextSupportCallback() {
                        @Override
                        public void onSuccess(ContextSupportInfo supportInfo) throws RemoteException {

                            Bundle bundle = new Bundle();
                            bundle.putString(ControlConnectionManager.CLIENT_PLUGIN_ID, receiverId);
                            ArrayList<String> controllers = new ArrayList<String>();
                            for (int i = 1; i < tapSession.size(); i++) {
                                controllers.add(tapSession.get(i));
                            }
                            bundle.putStringArrayList(ControlConnectionManager.SERVER_PLUGIN_ID, controllers);
                            bundle.putString(ControlConnectionManager.CONNECTION_CONTROL, ControlConnectionManager.CONFIG_INIT);

                            contextHandler.contextRequest(receiverId, IControlMessage.CONTEXT_TYPE, bundle,
                                    new IContextRequestCallback.Stub() {
                                        @Override
                                        public void onSuccess(ContextResult contextEvent) throws RemoteException {
                                            syncReceivers();
                                            done("Interaction set up successful");
                                        }

                                        @Override
                                        public void onFailure(final String s, final int i) throws RemoteException {
                                            done("Interaction setup failed: " + s + " | Error code: " + i);
                                            Log.w(TAG, "Call was unsuccessful! Message: " + s + " | Error code: "
                                                    + i);
                                            stopControlling(receiverId);
                                        }
                                    });
                        }

                        @Override
                        public void onFailure(String message, int errorCode) throws RemoteException {
                            done("error on connection");
                            Log.e(TAG, "error on connecting: " + message);
                        }
                    }
            );
        } else {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    done("Invalid device selection");
                }
            });
        }

    }

    private void connect(final String receiverId, final ContextSupportCallback callback) {
        if (contextHandler == null || !DynamixConnector.isConnected()) {
            contextHandler = null;
            try {
                ISessionCallback.Stub sessionCallback = new ISessionCallback.Stub() {
                    @Override
                    public void onSuccess(DynamixFacade iDynamixFacade) throws RemoteException {
                        dynamix = iDynamixFacade;
                        dynamix.createContextHandler(new ContextHandlerCallback() {
                            @Override
                            public void onSuccess(ContextHandler handler) throws RemoteException {
                                contextHandler = handler;
                                try {
                                    addSupport(receiverId, callback);
                                } catch (RemoteException e) {
                                    callback.onFailure(e.toString(), ErrorCodes.REQUEST_FAILED);
                                }
                            }

                            @Override
                            public void onFailure(String message, int errorCode) throws RemoteException {
                                done("failure to connect to create Context Handler");
                                Log.e(TAG, message);
                            }
                        });
                    }

                    @Override
                    public void onFailure(String s, int i) throws RemoteException {
                        done("failure to connect to Dynamix");
                        Log.e(TAG, s);
                    }
                };
                if (!DynamixConnector.isConnected())
                    DynamixConnector.openConnection(Tap2Interact.this, true, null, sessionCallback);
                else if (dynamix != null)
                    sessionCallback.onSuccess(dynamix);
            } catch (RemoteException e) {
                done("Unknown Error: " + e);
                e.printStackTrace();
            }
        } else
            try {
                addSupport(receiverId, callback);
            } catch (RemoteException e) {
                done("Unknown Error: " + e);
                e.printStackTrace();
            }

    }

    private void addSupport(final String receiverId, final ContextSupportCallback callback) throws RemoteException {
        contextHandler.addContextSupport(receiverId, IControlMessage.CONTEXT_TYPE, new ContextSupportCallback() {
            @Override
            public void onSuccess(ContextSupportInfo supportInfo) throws RemoteException {
                Log.i(TAG, "support added for: " + supportInfo);
                activeReceiversMap.put(receiverId, supportInfo);
                callback.onSuccess(supportInfo);

            }

            @Override
            public void onFailure(String message, int errorCode) throws RemoteException {
                Log.w(TAG,
                        "Call was unsuccessful! Message: " + message + " | Error code: "
                                + errorCode);
                callback.onFailure(message, errorCode);
            }
        });
    }


    private void stopControlling() {
        Bundle stopBundle = new Bundle();
        stopBundle.putString(ControlConnectionManager.CONNECTION_CONTROL, ControlConnectionManager.CONFIG_STOP);
        try {
            for (String activeReceiver : activeReceivers) {
                contextHandler.contextRequest(activeReceiver, IControlMessage.CONTEXT_TYPE, stopBundle);
                activeReceiversMap.remove(activeReceiver);
            }
            Thread.sleep(500);
            if(contextHandler != null)
                dynamix.removeContextHandler(contextHandler.getContextHandler());
            contextHandler = null;
            done("All Interactions Canceled");
        } catch (RemoteException e) {
            done("Unknown Error: " + e);
            e.printStackTrace();
        } catch (InterruptedException e) {
            done("Unknown Error: " + e);
            e.printStackTrace();
        } finally {
            syncReceivers();
        }
    }

    private void stopControlling(String receiverId) {
        Bundle stopBundle = new Bundle();
        stopBundle.putString(ControlConnectionManager.CONNECTION_CONTROL, ControlConnectionManager.CONFIG_STOP);
        try {
            contextHandler.contextRequest(receiverId, IControlMessage.CONTEXT_TYPE, stopBundle);
            Thread.sleep(500);
            contextHandler.removeContextSupport(activeReceiversMap.get(receiverId));
            activeReceiversMap.remove(receiverId);
            if (contextHandler.getContextSupport().getContextSupportInfo().size() == 0) {
                dynamix.removeContextHandler(contextHandler.getContextHandler());
                contextHandler = null;
            }
        } catch (RemoteException e) {
            done("Unknown Error: " + e);
            e.printStackTrace();
        } catch (InterruptedException e) {
            done("Unknown Error: " + e);
            e.printStackTrace();
        } finally {
            syncReceivers();
        }

    }

    private String getIdFrombytes(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%02X", b));
        }
        return sb.toString();
    }

    @Override
    protected void onResume() {
        super.onResume();
        String[][] techList = new String[][]{};
        mNfcAdapter.enableForegroundDispatch(this, mPendingIntent, mFilters, techList);
    }

    @Override
    protected void onPause() {
        mNfcAdapter.disableForegroundDispatch(this);
        super.onPause();
    }

    @Override
    protected void onStop() {
        stopControlling();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.i(TAG, "ON DESTROY for: Dynamix Simple Logger (A1)");
        /*
		 * Always remove our listener and unbind so we don't leak our service connection
		 */
        try {
            if(dynamix!=null && contextHandler != null)
            dynamix.removeContextHandler(contextHandler.getContextHandler());
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        if (DynamixConnector.isConnected()) {
            DynamixConnector.closeConnection();
        }
        if (contextHandler != null) {
            contextHandler = null;
        }
        super.onDestroy();
    }

    private void syncReceivers() {
        activeReceivers.clear();
        activeReceivers.addAll(activeReceiversMap.keySet());
        handler.post(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
            }
        });
    }

    public void playVideo(View view) {
        if(contextHandler == null){
            done("Not Connected");
            return;
        }
        Log.i(TAG, "checking for media devices");
        try {
            contextHandler.addContextSupport("org.ambientdynamix.contextplugins.ambientmedia","org.ambientdynamix.contextplugins.ambientmedia.discovery",new ContextSupportCallback(){
                @Override
                public void onSuccess(final ContextSupportInfo supportInfoDiscovery) throws RemoteException {
                    contextHandler.contextRequest("org.ambientdynamix.contextplugins.ambientmedia",
                            "org.ambientdynamix.contextplugins.ambientmedia.discovery", new IContextRequestCallback.Stub() {

                                @Override
                                public void onSuccess(ContextResult contextResult) throws RemoteException {
                                    if (contextResult.getIContextInfo() instanceof IMediaDeviceInfoList) {
                                        final IMediaDeviceInfoList result = (IMediaDeviceInfoList) contextResult.getIContextInfo();
                                        for (IMediaDeviceInfo deviceInfo : result.getMediaDevices()) {
                                            Log.i(TAG, "found device: " + deviceInfo.getDisplayName() + " " + deviceInfo.getType());
                                            final Bundle bundle = new Bundle();
                                            bundle.putString("interaction_type", "DeviceCommand");
                                            bundle.putString("command", "play");
                                            //                                bundle.putString("uri", "http://www.youtube.com/watch?v=lX6JcybgDFo");
//                                            bundle.putString("uri", "http://10.0.1.110:8888/hawpardragons.mov");
//                                                            bundle.putString("uri","http://hdwallpaper2013.com/wp-content/uploads/2013/02/Download-Flower-Background-Images-HD-Wallpaper-1080x607.jpg");
                                                            bundle.putString("uri","http://www.youtube.com/watch?v=-BkG5xTGvh0");
                                            bundle.putString("device_id", deviceInfo.getId());
                                            try {
                                                contextHandler.addContextSupport("org.ambientdynamix.contextplugins.ambientmedia", "org.ambientdynamix.contextplugins.ambientmedia.interaction", new ContextSupportCallback(){
                                                    @Override
                                                    public void onSuccess(final ContextSupportInfo supportInfoInteraction) throws RemoteException {
                                                        contextHandler.contextRequest("org.ambientdynamix.contextplugins.ambientmedia", "org.ambientdynamix.contextplugins.ambientmedia.interaction", bundle, new IContextRequestCallback.Stub() {
                                                            @Override
                                                            public void onSuccess(ContextResult contextResult) throws RemoteException {
                                                                Log.i(TAG, "sucessfully started playback of: " + bundle.getString("uri"));
//                                                                contextHandler.removeContextSupport(supportInfoDiscovery);
//                                                                contextHandler.removeContextSupport(supportInfoInteraction);
                                                            }

                                                            @Override
                                                            public void onFailure(String s, int i) throws RemoteException {
                                                                Log.i(TAG, "error on playback of: " + bundle.getString("uri") + " " + s);
                                                            }
                                                        });
                                                    }

                                                    @Override
                                                    public void onFailure(String message, int errorCode) throws RemoteException {
                                                        super.onFailure(message, errorCode);
                                                    }
                                                });
                                            } catch (RemoteException e) {
                                                e.printStackTrace();
                                            }
                                        }

                                    } else
                                        Log.i(TAG, "No Media device found for playback");
                                }

                                @Override
                                public void onFailure(String s, int i) throws RemoteException {
                                    Log.e(TAG, s);
                                }
                            });
                }

                @Override
                public void onFailure(String message, int errorCode) throws RemoteException {
                    super.onFailure(message, errorCode);
                }
            });

        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

}

